#Ecrire un programme qui affiche :
print("Bonjour les simplonniens")

#Ecrire un programme qui permet de saisir le nom de l'utilisateur 
#et de renvoyer "Bonjour", suivi de ce nom.
nom = input("Ecris ton nom :")
print("Bonjour", nom)

#Écrire un programme qui demande à l'utilisateur la saisie de a et b 
#et affiche la somme de a et de b.
a = input("Saisir a :")
b = input("Saisir b :")
print(a+b)

#Écrire un programme qui affiche une suite de 12 nombres 
#dont chaque terme soit égal au triple du terme précédent.
i = 1
j = i * 3
while i <= 12:
    j *= 3
    print(j)
    i += 1

#Écrire un programme qui affiche la suite de symboles suivante
for i in range(7):
	print("*" *i)